package solve;

public class Wall extends Piece {

	private static Wall instance;
	
	private Wall() {
		super(false, null);
	}
	
	public static Wall get() {
		if (instance == null)
			instance = new Wall();
		return instance;
	}
	
	@Override
	public char symbol() {
		return 'W';
	}
	
}
