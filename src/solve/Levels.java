package solve;


public class Levels {
	
	public static void main(String[] args) {
		new Solution(B33).interactive();
	}
	
	public static final int[][] B31 = {
		{32, 0, 0, 0},
		{0, 0, 69, 32},
		{369, 0, 21, 0}
	};
	
	public static final int[][] B32 = {
		{0, 0, 32, 0},
		{29, 0, 34, 329},
		{0, 6, 381, 0},
	};
	
	
	public static final int[][] B33 = {
		{29, 36, 36, 0},
		{0, 0, 0, 2},
		{41, 349, 0, 0}
	};
	
	public static final int[][] B36 = {
		{329, 0, 36, 0},
		{0, 5, 0, 0},
		{419, 0, 5, 0},
		{0, 0, 0, 0}
	};
	
	
	public static final int[][] B39 = {
		{0, 0, 1, 29},
		{0, 5, 369, 0},
		{0, 34, 5, 0},
		{8, 0, 0, 0},
	};
	
	public static final int[][] B54 = {
		{6, 6, 0, 0},
		{0, 21, 0, 329},
		{34, 0, 0, 0},
		{32, 34, 0, 94},
	};
	
	public static final int[][] B91 = {
		{0, 891, 0, 4, 0, 0},
		{0, 389, 38, 0, 38, 0},
	};
	
	public static final int[][] B94 = {
		{0, 0, 369, 0, 819, 0},
		{8, 36, 0, 0, 34, 0},
	};
	
}
