package solve;

public enum Dir {

	UP(1, 0, -1, 'v'),
	DOWN(0, 0, 1, '^'),
	LEFT(3, -1, 0, '>'),
	RIGHT(2, 1, 0, '<');
	
	public final int x;
	public final int y;
	
	public final char symbol;
	
	private final int opp;
	private Dir opposite;
	private Dir(int opp, int x, int y, char symbol) {
		this.opp = opp;
		this.x = x;
		this.y = y;
		this.symbol = symbol;
	}
	
	public Dir opposite() {
		if (opposite == null) {
			this.opposite = Dir.values()[opp];
		}
		return opposite;
	}
}
