package solve;

import java.awt.Font;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

public class Solution {

	private int checked;

	public final long solveTime;

	public final Board original;
	public final Board solution;
	public final ArrayList<Board> trace;

	private static final AtomicInteger threadID = new AtomicInteger(0);
	private static final ConcurrentHashMap<Integer, Solution> threaded = new ConcurrentHashMap<>();
	private static final LinkedBlockingQueue<Solution> completed = new LinkedBlockingQueue<>();

	public Solution(int[][] level) {		
		original = new Board(level);

		long time = System.nanoTime(); 
		solution = solve();
		solveTime = System.nanoTime() - time;
		System.out.printf("%.2f ms", solveTime / 1.0e6);
		trace = trace();
	}

	public static int thread(int[][] level) {
		final int id = threadID.getAndIncrement();
		new Thread() {
			public void run() {
				Solution solution = new Solution(level);
				threaded.put(id, solution);

			};
		}.start();
		return id;
	}

	public boolean isComplete(int id) {
		return threaded.containsKey(id);
	}

	public Solution getSolution(int id) {
		return threaded.get(id);
	}

	public Solution getNextCompleted() {
		try {
			return completed.take();
		} catch (InterruptedException e) {
			return null;
		}
	}

	public String toString() {
		StringBuilder build = new StringBuilder();

		if (solution == null) {
			build.append("No solution after " + checked + " moves checked\n");
		} else {
			build.append("Solved in " + (trace.size() - 1) + " moves after " + checked + " possibilites checked");
			build.append('\n');
			build.append(trace.get(0));
			build.append('\n');
			for (int i = 1; i < trace.size(); i++) {
				build.append(trace.get(i).move);
				build.append('\n');
				build.append(trace.get(i));
				build.append('\n');
			}
		}

		return build.toString();
	}

	public void interactive() {
		UIManager.put("OptionPane.messageFont", new Font(Font.MONOSPACED, Font.PLAIN, 14));

		if (solution == null) {
			JOptionPane.showMessageDialog(null, "No solution after " + checked + " moves checked");
			return;
		}

		for (int i = 0; i < trace.size(); i++) {
			String message = i + 1 == trace.size() ? "SOLUTION" : trace.get(i + 1).move.toString();
			message += "\n\n" + trace.get(i) + "\n";

			String title = "Move " + (i + 1) + " of " + (trace.size() - 1);
			title += "  (" + checked + " positions checked)";
			if (i + 1 == trace.size()) title = "Solved in " + (trace.size() - 1) + " moves";

			int selection = JOptionPane.showOptionDialog(null, message, title,
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null,
					new String[] {" Back ", " Next ", " >> 10 "}, " Next ");

			switch (selection) {
			case JOptionPane.CANCEL_OPTION:
				if (i + 1 == trace.size())
					break;
				i += 9;
				if (i + 2 >= trace.size()) 
					i = trace.size() - 2;
				break;
			case JOptionPane.YES_OPTION:
				i -= 2;
				if (i < 0)
					i = -1;
				break;
			case JOptionPane.CLOSED_OPTION:
				return;
			}
		}
	}

	public Board solve() {
		if (original.win()) return original;

		HashSet<Board> seen = new HashSet<>();
		ArrayList<Board> look = new ArrayList<>();

		look.add(original);
		seen.add(original);

		while (!look.isEmpty()) {
			Board test = look.remove(0);
			for (Dir dir : Dir.values()) {
				Board move = test.move(dir);
				if (move != null && !seen.contains(move)) {
					checked++;
					if (move.win()) {
						return move;
					} else {
						look.add(move);
						seen.add(move);
					}
				}
			}
		}

		//All moves exhausted, no solution found
		return null;
	}

	//Go back through the lineage of the solution to trace moves
	public ArrayList<Board> trace() {
		if (solution == null) return null;
		ArrayList<Board> trace = new ArrayList<>();
		Board tmp = solution;
		do {
			trace.add(0, tmp);
			tmp = tmp.parent;
		} while (tmp != null);
		return trace;
	}

}
