package solve;

import java.util.Arrays;

public class Board {
	
	public Board parent;
	public Dir move;

	private int x, y;
	private String divider;
	
	private int bx = -1;
	private int by = -1;
	
	private Piece[][] big;
	private Piece[][] little;
	
	public static final int IGNORE = '3';
	public static final int BALL = '1';
	public static final int WALL = '5';
	public static final int METAL = '9';
	
	public static final int DOWN = 2;
	public static final int LEFT = 4;
	public static final int RIGHT = 6;
	public static final int UP = 8;
	
	public static final Dir[] dirs = {null, null,
		Dir.DOWN, null,
		Dir.LEFT, null,
		Dir.RIGHT, null,
		Dir.UP, null,
	};
	
	private Board(int bx, int by, Piece[][] big, Piece[][] little, Board parent, Dir move, String divider) {
		x = big[0].length;
		y = big.length;
		
		this.bx = bx;
		this.by = by;
		
		this.big = big;
		this.little = little;
		
		this.parent = parent;
		this.move = move;
		
		this.divider = divider;
	}
	
	public Board(int[][] board) {
		x = board[0].length;
		y = board.length;
		
		if (x == 0 || y == 0) {
			throw new IllegalArgumentException("Board dimension cannot be zero");
		}
		
		divider = "";
		for (int i = 0; i < x; i++)
			divider += "---+";
		divider = divider.substring(0, divider.length() - 1) + "\n";
		
		big = new Piece[y][x];
		little = new Piece[y][x];
		
		int bigMetal = 0;
		int littleMetal = 0;
		
		for (int y = 0; y < this.y; y++) {
			for (int x = 0; x < this.x; x++) {
				if (board[y][x] == 0) {
					continue;
				}
				
				String s = board[y][x] + "";
				
				if (s.indexOf(WALL) >= 0) {
					big[y][x] = Wall.get();
					continue;
				}
				
				int ball = s.indexOf(BALL);
				if (ball >= 0) {
					bx = x;
					by = y;
					s = s.substring(0, ball) + s.substring(ball + 1, s.length());
				}
				
				int metal = s.indexOf(METAL);
				if (metal >= 0) {
					s = s.substring(0, metal) + s.substring(metal + 1, s.length());
				}				
				
				if (s.isEmpty()) continue;
				if (s.charAt(0) != IGNORE) {
					big[y][x] = new Piece(metal >= 0, dirs[s.charAt(0) - '0']);
					bigMetal += metal >= 0 ? 1 : 0;
				}
				s = s.substring(1);
				
				if (s.isEmpty()) continue;
				little[y][x] = new Piece(metal >= 0, dirs[s.charAt(0) - '0']);
				littleMetal += metal >= 0 ? 1 : 0;
			}
		}
		
		//Validate
		if (bx < 0) {
			throw new IllegalArgumentException("No ball location");
		}
		if (bigMetal != 1) {
			throw new IllegalArgumentException("1 big metal box required; " + bigMetal + " provided");
		}
		if (littleMetal != 1) {
			throw new IllegalArgumentException("1 little metal box required; " + littleMetal + " provided");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(big);
		result = prime * result + Arrays.deepHashCode(little);
		result = prime * result + bx;
		result = prime * result + by;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		if (!Arrays.deepEquals(big, other.big))
			return false;
		if (bx != other.bx)
			return false;
		if (by != other.by)
			return false;
		if (!Arrays.deepEquals(little, other.little))
			return false;
		return true;
	}

	public Board move(Dir dir) {
		// The ordering of the below checks is very important.
		// Some checks later on rely on guarantees made by prior ones.
		
		//Move off board check
		int dx = bx + dir.x;
		if (dx < 0 || dx >= x)
			return null;
		int dy = by + dir.y;
		if (dy < 0 || dy >= y)
			return null;
		
		//Destination pieces
		Piece dBig = big[dy][dx];
		Piece dLittle = little[dy][dx];
		
		//Running into a blocked space check
		if (dBig != null && dBig.in != dir)
			return null;
		if (dLittle != null && dLittle.in != dir)
			return null;
		
		//Current pieces
		Piece cBig = big[by][bx];
		Piece cLittle = little[by][bx];

		
		//Simple ball move out of spot; no need to clone arrays
		if ((cBig == null || cBig.out == dir) && (cLittle == null || cLittle.out == dir))
			return new Board(dx, dy, big, little, this, dir, divider);
		
		//Can't move big into little or big
		if (cBig != null && cBig.out != dir && (dLittle != null || dBig != null))
			return null;
		
		//Can't move little into little or blocked big
		if (cLittle != null && (dLittle != null || (dBig != null && dBig.in != dir)))
			return null;
		
		//The move is legal and moves one or two pieces
		Board board = this.clone(dx, dy, dir);
		
		//Move little
		board.little[dy][dx] = cLittle;
		board.little[by][bx] = null;
		
		//Move big if blocking
		if (cBig != null && cBig.out != dir) {
			board.big[dy][dx] = cBig;
			board.big[by][bx] = null;
		}
		
		return board;
	}

	public boolean win() {
		return big[by][bx] != null && little[by][bx] != null
				&& big[by][bx].metal && little[by][bx].metal;
	}
	
	@Override
	public String toString() {
		StringBuilder build = new StringBuilder();
		for (int y = 0; y < this.y; y++) {
			for (int x = 0; x < this.x; x++) {
				build.append(big[y][x] == null ? ' ' : big[y][x].symbol());
				build.append(little[y][x] == null ? ' ' : little[y][x].symbol());
				build.append(by == y && bx == x ? 'O' : ' ');
				if (x + 1 < this.x) {
					build.append('|');
				} else {
					build.append('\n');
				}
			}
			
			if (y + 1 < this.y) {
				build.append(divider);
			}
		}
		return build.toString();
	}
	
	public Board clone(int dx, int dy, Dir move) {		
		Piece[][] big = new Piece[y][x];
		Piece[][] little = new Piece[y][x];
		for (int i = 0; i < y; i++) {
			 System.arraycopy(this.big[i], 0, big[i], 0, x);
			 System.arraycopy(this.little[i], 0, little[i], 0, x);
		}
		return new Board(dx, dy, big, little, this, move, divider);
	}
	
}
