package solve;

public class Piece {

	public final boolean metal;
	public final Dir in;
	public final Dir out;
	
	public Piece(boolean metal, Dir opening) {
		this.metal = metal;
		this.out = opening;
		this.in = opening == null ? null : opening.opposite();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((in == null) ? 0 : in.hashCode());
		result = prime * result + (metal ? 1231 : 1237);
		result = prime * result + ((out == null) ? 0 : out.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Piece other = (Piece) obj;
		if (in != other.in)
			return false;
		if (metal != other.metal)
			return false;
		if (out != other.out)
			return false;
		return true;
	}

	public char symbol() {
		return out.symbol;
	}
	
}
